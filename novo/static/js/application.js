$(document).ready(function() {

  $("#fullpage .sectionFull").find("[data-animate]").each(function() {

    var animacao = $(this).data("animate");

    $(this).addClass("animated fadeOut");
    $(this).removeClass(animacao);
  });

  var autoScrolling = true;
  var fitToSection = true;
  var isMobile = window.matchMedia("only screen and (max-width: 760px)");

  // Mobile
  if (isMobile.matches) {
    autoScrolling = false;
    fitToSection = false;

    $("#listagem,#programa,#sorteios").addClass("fp-auto-height");
  }

  $(".anchor-effect").click(function() {
    var anchor = $(this).data("anchor");

    $('html, body').animate({
        scrollTop: $(anchor).offset().top
    }, 500);
    return false;
  });

  // FullPage

  function createFullPage() {
    $('#fullpage').fullpage({
      scrollBar: true,
      loopHorizontal: false,
  		sectionSelector:'.sectionFull',
      autoScrolling: autoScrolling,
      fitToSection: fitToSection,
      touchSensitivity: 9999,
      normalScrollElements: '.modal,.global_header,.footerGroup',
      afterLoad: function(anchorLink, index) {

        $("#fullpage .sectionFull.active").find("[data-animate]").each(function() {

          var animacao = $(this).data("animate");

          $(this).removeClass("fadeOut");
          $(this).addClass("animated " + animacao);
        });
      },
      onLeave: function(index, netIndex, direction) {

        $("#fullpage .sectionFull").find("[data-animate]").each(function() {

          var animacao = $(this).data("animate");

          $(this).addClass("fadeOut");
          $(this).removeClass(animacao);
        });

      },
      afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex){

        // $(".sectionFull.active .fp-controlArrow").prop("id", "current-slide-" + slideAnchor);

        $("#fullpage .sectionFull.active").find("[data-animate]").each(function() {

          var animacao = $(this).data("animate");

          $(this).removeClass("fadeOut animacaoRapida");
          $(this).addClass("animated " + animacao);
        });

      },
      onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {

        // var currentSlide = $('.fp-section.active').find('.fp-slide.active');
        // var nextSlideInside = $('.fp-section.active').find('.fp-slide').eq(nextSlideIndex);
        var proximoIndice = nextSlideIndex + 1;
        var proximoSlide = ".slide-" + proximoIndice;

          $('.fp-section.active').find(proximoSlide).find("[data-animate]").each(function() {

            var animacao = $(this).data("animate");

            $(this).addClass("fadeOut animacaoRapida");
            $(this).removeClass(animacao);
          });

        if(nextSlideIndex == 1 && anchorLink == 'conheca-melhor-o-programa') {
          $.fn.fullpage.silentMoveTo('conheca-melhor-o-programa', 3);
        }
        if(nextSlideIndex == 1 && anchorLink == 'conheca-melhor-o-programa' && direction == 'left') {
          $.fn.fullpage.silentMoveTo('conheca-melhor-o-programa', 0);
        }
      }
    });
  }

  createFullPage();


  // Desktop
  if (!isMobile.matches) {

    var novas_caracteristicas = $('<div id="caracteristicas" class="sectionFull" data-anchor="caracteristicas"></div>');
    var novo_programa = $('<div id="programa" class="sectionFull" data-anchor="programa"></div>');

    novas_caracteristicas.insertAfter("#listagem");
    novo_programa.insertAfter("#sorteios");

    $("#caracteristicas_1 .slide").appendTo("#caracteristicas");
    $("#caracteristicas_2 .slide").appendTo("#caracteristicas");
    $("#caracteristicas_3 .slide").appendTo("#caracteristicas");

    $("#programa_1 .slide").appendTo("#programa");
    $("#programa_2 .slide").appendTo("#programa");
    $("#programa_3 .slide").appendTo("#programa");

    $("#caracteristicas_1,#caracteristicas_2,#caracteristicas_3").detach();
    $("#programa_1,#programa_2,#programa_3").detach();
    
    $.fn.fullpage.destroy('all');
    createFullPage();
    
  }

});
