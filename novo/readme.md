# Samsung Template
> Samsung development patterns to http://www.samsung.com.br

## Folder structure
```
|-project-root
  |-static
    |-css
      |-style.css (project css styles)
    |-fonts (samsung fonts, can't be linked directly from .com)
    |-images (project images)
    |-js
      |-script.js (project javascript routines)
  |-index.html
  |-readme.md (this doc. don't upload to site environment)
  |-readme.html (this doc. don't upload to site environment)
```

## Steps to init project
Change any code starts with **TODO:**
```
- HTML:
<link rel="canonical" href="TODO: PROJECT FULL SITE URL">
<meta name="twitter:url" content="TODO: PROJECT FULL SITE URL">
<meta property="og:url" content="TODO: PROJECT FULL SITE URL">
<meta itemprop="url" content="TODO: PROJECT FULL SITE URL">
...
<div id="content">
    <!-- TODO: BEGIN PROJECT HTML CONTENT -->
    <h1>CONTEÚDO DO SITE</h1>
    <!-- TODO: END PROJECT HTML CONTENT -->
</div>
```

```
- CSS:
<!-- TODO: BEGIN ADD PROJECT CSS FILES HERE -->
<link rel="stylesheet" href="static/css/style.css"/>
<!-- TODO: END ADD PROJECT CSS FILES HERE -->
```

```
- JS:
<!-- TODO: BEGIN ADD PROJECT JAVASCRIPT FILES HERE -->
<script type="text/javascript" src="static/js/script.js"></script>
<!-- TODO: BEGIN ADD PROJECT JAVASCRIPT FILES HERE -->
```

## Project Highlights
1. jQuery is already exists inside http://www.samsung.com/common/next/js/samsung.min.js (v1.8.3)