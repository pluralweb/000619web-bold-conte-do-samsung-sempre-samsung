var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    jsmin = require('gulp-jsmin'),
    connect = require('gulp-connect'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');

// LiveReload
gulp.task('connect', function() {
  connect.server({
    root: 'app',
    livereload: true
  });
});

// Stylus
gulp.task('stylus', function(){
  gulp.src('css/application.styl')
  .pipe(plumber())
  // Minify
  .pipe(stylus({
    compress: false
   }))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('css'))
  .pipe(connect.reload());
});

// JavaScript
gulp.task('js', function(){
  gulp.src('js/application.js')
    .pipe(plumber())
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('js'))
    .pipe(connect.reload());
});

// Watch
gulp.task('watch', function () {
  gulp.watch(['./css/*.styl', './css/blocks/*.styl', './css/responsive/*.styl'], ['stylus']);
  gulp.watch(['./js/application.js'], ['js']);
});

// Default
gulp.task('default', ['connect', 'stylus', 'js', 'watch']);
