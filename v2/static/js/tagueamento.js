﻿
clickCatalyst = function(n) {
    for (i in n) s[i] = n[i];
    s.t();
    for (i in n) s[i] = ""
};

$(function() {

    /* obj global */
    omni = {
        channel: 'br:campaign',
        prop1: 'br',
        prop2: 'br',
        prop3: 'br:campaign',
        prop4: 'br:campaign:seda',
        prop5: 'br:campaing:seda:programagalaxyparasempre',
        hier1: 'br>campaign>seda>programagalaxyparasempre>home',
        pageName: 'br:campaign:seda:programagalaxyparasempre:home'
    }

    clickCatalyst(omni);

    /* obj scroll */
    $(window).on('hashchange', function(e) {
        hash = document.location.hash.split('#')[1];
        if (hash != 'programa-galaxy-para-sempre') {
            omni.hier1 = 'br>campaign>seda>programagalaxyparasempre>' + hash;
            omni.pageName = 'br:campaign:seda:programagalaxyparasempre:' + hash;
        } else {
            omni.hier1 = 'br>campaign>seda>programagalaxyparasempre>home';
            omni.pageName = 'br:campaign:seda:programagalaxyparasempre:home';
        }
        clickCatalyst(omni);
    });

    //botao condicoes-do-regulamento
    $('.botao_condicoes_regulamento').on("click", function() {
        omni.hier1 = 'br>campaign>seda>programagalaxyparasempre>perguntas-frequentes>botao-condicoes-do-regulamento';
        omni.pageName = 'br:campaign:seda:programagalaxyparasempre:perguntas-frequentes:botao-condicoes-do-regulamento';

        clickCatalyst(omni);
        omni.eVar5 = '';
    });


    //botao botao regulamento|FAC - celulares-galaxy-todo-ano-sem-custo-adicional
    $('#topo_embalagem a').on("click", function() {

        omni.hier1 = 'br>campaign>seda>programagalaxyparasempre>celulares-galaxy-todo-ano-sem-custo-adicional>botao-' + $(this).text();
        omni.pageName = 'br:campaign:seda:programagalaxyparasempre:celulares-galaxy-todo-ano-sem-custo-adicional:botao-' + $(this).text();

        clickCatalyst(omni);
        omni.eVar5 = '';
    });

    $('#perguntas_frequentes #accordion .panel-heading').on('click', function(event) {
			var el = $(this);
			var a = el.find('a');
			var colapse = a.attr('aria-expanded');
			var href = a.attr('href').split('#collapse')[1];

			if(colapse == 'false'){
				sendClickCode('faq','openFAQ"+href+"_button_click');
			}else{
				sendClickCode('faq','closeFAQ"+href+"_button_click');
			}

		})
    $('#perguntas_frequentes .panel-scrolling button.down').on('click', function(event) {
			sendClickCode('faq','scrollDown_button_click');
		})
    $('#perguntas_frequentes .panel-scrolling button.up').on('click', function(event) {
			sendClickCode('faq','scrollUp_button_click');
		})
    $('.fp-controlArrow.fp-prev').on('click', function(event) {
        event.preventDefault();
        hash = document.location.hash.split('#')[1];

				if (hash == 'caracteristicas/2') {
						sendClickCode('caracteristicas2','goToPage1_button_click');
				}
				if (hash == 'caracteristicas/1') {
						sendClickCode('caracteristicas1','goToPage0_button_click');
				}

				if (hash == 'programa/2') {
						sendClickCode('programa2','goToPage1_button_click');
				}
				if (hash == 'programa/1') {
						sendClickCode('programa1','goToPage0_button_click');
				}

    });
    $('.fp-controlArrow.fp-next').on('click', function(event) {
        event.preventDefault();
        hash = document.location.hash.split('#')[1];
				if (hash == 'caracteristicas/1') {
            sendClickCode('caracteristicas1','goToPage2_button_click');
        }
				if (hash == 'caracteristicas') {
            sendClickCode('caracteristicas','goToPage1_button_click');
        }

				if (hash == 'programa/1') {
            sendClickCode('programa1','goToPage2_button_click');
        }
				if (hash == 'programa') {
            sendClickCode('programa','goToPage1_button_click');
        }
    });


})
