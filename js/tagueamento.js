﻿clickCatalyst = function (n) { for (i in n) s[i] = n[i]; s.t(); for (i in n) s[i] = "" };

$(function () {

	/* obj global */
	omni = {
		channel  : 'br:campaign',
		prop1    : 'br',
		prop2    : 'br',
		prop3    : 'br:campaign',
		prop4    : 'br:campaign:seda',
		prop5	   : 'br:campaing:seda:programagalaxyparasempre',
		hier1    : 'br>campaign>seda>programagalaxyparasempre>home',
		pageName : 'br:campaign:seda:programagalaxyparasempre:home'
	}

	clickCatalyst(omni);

	/* obj scroll */
	$(window).on('hashchange', function(e){
		hash=document.location.hash.split('#')[1];
		if(hash != 'programa-galaxy-para-sempre'){
			omni.hier1    = 'br>campaign>seda>programagalaxyparasempre>'+hash;
			omni.pageName = 'br:campaign:seda:programagalaxyparasempre:'+hash;
		}else{
			omni.hier1    = 'br>campaign>seda>programagalaxyparasempre>home';
			omni.pageName = 'br:campaign:seda:programagalaxyparasempre:home';
		}
		clickCatalyst(omni);
	});

//botao condicoes-do-regulamento
	$('.botao_condicoes_regulamento').on( "click", function() {
		omni.hier1    = 'br>campaign>seda>programagalaxyparasempre>perguntas-frequentes>botao-condicoes-do-regulamento';
		omni.pageName = 'br:campaign:seda:programagalaxyparasempre:perguntas-frequentes:botao-condicoes-do-regulamento';

		clickCatalyst(omni);
		omni.eVar5    = '';
	});


//botao botao regulamento|FAC - celulares-galaxy-todo-ano-sem-custo-adicional
	$('#topo_embalagem a').on( "click", function() {

		omni.hier1    = 'br>campaign>seda>programagalaxyparasempre>celulares-galaxy-todo-ano-sem-custo-adicional>botao-'+$(this).text();
		omni.pageName = 'br:campaign:seda:programagalaxyparasempre:celulares-galaxy-todo-ano-sem-custo-adicional:botao-'+$(this).text();

		clickCatalyst(omni); 
		omni.eVar5    = '';
	});
})
